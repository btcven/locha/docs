+++
title = "Official Locha Mesh Project Documentation"
description = ""
+++

# FAQ


## What is Locha?

It's a project aiming to design, develop, build and sell devices that allow a peer to peer encrypted communications using mesh networking protocols.

## What is Locha's vision?

A mesh network formed of devices smaller and cheaper than a mobile phone that are able to communicate even on the most harsh conditions like: internet censorship, compromised infraestructure, blackouts, earthquakes. Power consumption is low enough that a small battery can keep the device running for a two days and doesn't need to use internet to send messages between devices, but can be used as a way to reach isolated networks via Gateway Node, which is a more powerful device that can share internet connection free of charge or charging it's services using Bitcoin (Lighting network) payments, which is possible through an integrated wallet on the Locha Mobile App, allowing not only messaging but also sending and signing Bitcoin transactions to allow users to trade even in the most extreme situations where traditional banking is not capable to operate.

## What about the code?

Source code is free, allowing customization and code audits. It's built primarily on C++ Language. Gitlab repository below.

## How much does it cost?

Price ranges from 50 to 100 USD per unit, enabling a low entry barrier for low purchasing power potential users.
Each device can communicate even at 4 Km with a clear line of sight, and about 1 Km in common urban enviroments. A bigger and more powerful version would be able to communicate even further.

## What means mesh networking with these devices?

Devices can relay messages using a mesh protocol, communicating devices that are directily out of reach but indirectically accessible, easily building a network able to send messages within a city with just a few well placed devices. If a device is turned off, best route is updated to use another one, making it pretty resilient, specially when there are plenty devices connected.

## How do I use them to send messages?

Mobile App makes possible to send messages to uniquely identified devices that are totally unrelated to any private information (such as phone number or IP addresses), whose ID is generated with a "seed" (that allows to change devices keeping previous ID and contacts). Group chats are also a possibility.
